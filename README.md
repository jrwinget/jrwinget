### Hello! Welcome. 👋

<!--
**jrwinget/jrwinget** is a _special_ repository because its `README.md` (this file) appears on your GitLab profile.-->
I’m a full-time instructor and researcher, who uses advanced quantitative methods to improve how individuals and groups make judgments and decisions. I am also a part-time consultant, specializing in statistics and research methods, as well an author and invited speaker. I work to make data analysis easier, open, and fun. 
